"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""

def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float :
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    vat_tax_rate = 0.206

    if discount_rate > 1 or discount_rate < 0:
        return 1 / 0    # used to throw an exception

    # The discount rate is applied first, then the VAT is applied afterwards
    # over the item discounted value.
    # The result is then rounded to 2 digits after the decimal point.
    return float("%.2f" % (htc_cost * (1 - discount_rate) * (1 + vat_tax_rate)))


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """

    set_divisors = set()
    divisor = 2
    value_max = value

    while divisor <= value_max / 2:
        if value % divisor == 0:
            set_divisors.add(divisor)
            value = value / divisor
        else:
            divisor += 1

    if not set_divisors:
        return "PREMIER"
    return set_divisors
